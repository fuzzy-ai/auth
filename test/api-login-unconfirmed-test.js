// api-login-test.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const apiBatch = require('./api-batch')

const env = require('./config')

vows
  .describe('POST /login for unconfirmed user')
  .addBatch(apiBatch({
    'and we register a new user': {
      topic (auth, mock) {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/register',
          json: {
            email: 'loginuser@unit.test',
            password: 'aloud bole phony feign 94th'
          },
          headers: {
            authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
          }
        }

        async.parallel([
          callback =>
            mock.once('template', (name, paramString) => {
              let params
              try {
                params = JSON.parse(paramString)
              } catch (err) {
                callback(err)
              }
              callback(null, __guard__(params.data != null ? params.data.confirmation : undefined, x => x.code))
            }),
          callback =>
            request.post(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 202) {
                const code = response.statusCode
                const message = body != null ? body.message : undefined
                callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                callback(null, body)
              }
            })

        ], (err, results) => {
          if (err) {
            callback(err)
          } else {
            const [code, body] = Array.from(results)
            callback(null, body, code)
          }
        })
        return undefined
      },
      'it works' (err, body, code) {
        assert.ifError(err)
      },
      'and we login the user': {
        topic (results) {
          const { callback } = this
          const options = {
            url: 'http://localhost:2342/login',
            json: {
              email: 'loginuser@unit.test',
              password: 'aloud bole phony feign 94th'
            },
            headers: {
              authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              callback(err)
            } else if (response.statusCode !== 409) {
              const code = response.statusCode
              const message = body != null ? body.message : undefined
              callback(new Error(`Bad status code ${code}: ${message}`))
            } else {
              callback(null, body)
            }
          })
          return undefined
        },
        'it fails correctly' (err, results) {
          assert.ifError(err)
          assert.isString(results.status)
          assert.equal(results.status, 'error')
        }
      }
    }
  })).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
