// sendgrid-mock.js
// Copyright 2016 fuzzy.ai <evan@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const events = require('events')

const debug = require('debug')('auth:sendgrid-mock')

const JSON_TYPE = 'application/json; charset=utf-8'

const lists = [
  {
    id: 1,
    name: 'unit test',
    recipient_count: 1
  },
  {
    id: 2,
    name: 'other',
    recipient_count: 10
  }
]

const recipientsResults = {
  'error_count': 0,
  'error_indices': [],
  'new_count': 1,
  'persisted_recipients': [
    'testrecipient'
  ],
  'updated_count': 0,
  'errors': []
}

class SendgridServerMock extends events.EventEmitter {
  constructor (key) {
    super()
    const self = this
    debug(`Initializing with ${key}`)
    const server = http.createServer((request, response) => {
      debug(`${request.method} ${request.url}`)
      let body = '' // eslint-disable-line no-unused-vars
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      request.on('end', () => {
        const auth = request.headers.authorization
        debug(`auth=${auth}`)
        if (!auth || (auth !== `Bearer ${key}`)) {
          debug('Bad authorization error')
          return respond(403, {status: 'error', message: 'No access'})
        }

        switch (`${request.method} ${request.url}`) {
          case 'GET /v3/contactdb/lists':
            return respond(200, {lists})
          case 'POST /v3/contactdb/recipients':
            return respond(201, recipientsResults)
          case 'POST /v3/contactdb/lists/1/recipients':
            return respond(201, null)
          default:
            const verb = request.method
            const { url } = request
            // If we get here, no route found
            return respond(404, {status: 'error', message: `Cannot ${verb} ${url}`})
        }
      })
      return server.on('request', (req, res) => self.emit('request', req, res))
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(4815)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = SendgridServerMock
