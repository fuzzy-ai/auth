// api-register-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const apiBatch = require('./api-batch')
const env = require('./config')

vows
  .describe('POST /register')
  .addBatch(apiBatch({
    'and we register a new user': {
      topic (auth, mock) {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/register',
          json: {
            email: 'fakeuser@unit.test',
            password: 'aloud bole phony feign 94th'
          },
          headers: {
            authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
          }
        }

        async.parallel([
          callback =>
            mock.once('template', (name, paramString) => {
              let params
              try {
                params = JSON.parse(paramString)
              } catch (err) {
                callback(err)
              }
              callback(null, [name, params])
            }),
          callback =>
            request.post(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 202) {
                const code = response.statusCode
                const message = body != null ? body.message : undefined
                callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                callback(null, body)
              }
            })

        ], (err, results) => {
          if (err) {
            callback(err)
          } else {
            const [[name, params], body] = results
            callback(null, body, name, params)
          }
        })
        return undefined
      },
      'it works' (err, body, name, params) {
        assert.ifError(err)
      },
      'body looks correct' (err, body, name, params) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.status)
        assert.equal('awaiting email confirmation', body.status)
      },
      'mailer was called' (err, body, name, params) {
        assert.ifError(err)
        assert.equal('confirmation', name)
        assert.isObject(params)
        assert.isObject(params.data)
        assert.isObject(params.data.confirmation)
        assert.isString(params.data.confirmation.code)
      },
      'and we confirm the registration': {
        topic (body, name, params) {
          const { callback } = this
          const options = {
            url: 'http://localhost:2342/confirm',
            json: {
              code: params.data.confirmation.code
            },
            headers: {
              authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              callback(err)
            } else if (response.statusCode !== 200) {
              const code = response.statusCode
              const message = body != null ? body.message : undefined
              callback(new Error(`Bad status code ${code}: ${message}`))
            } else {
              callback(null, body)
            }
          })
          return undefined
        },
        'it works' (err, results) {
          assert.ifError(err)
          assert.isObject(results)
          assert.isObject(results.user)
          assert.isString(results.token)
        },
        'user has an id' (err, results) {
          assert.ifError(err)
          assert.isString(results.user.id)
        },
        'user has the correct email address' (err, results) {
          assert.ifError(err)
          assert.equal(results.user.email, 'fakeuser@unit.test')
        },
        'user does not have a password property' (err, results) {
          assert.ifError(err)
          assert.notInclude(results.user, 'password')
          assert.notInclude(results.user, 'passwordHash')
        },
        'user has a default token' (err, results) {
          assert.ifError(err)
          assert.isString(results.user.defaultToken)
        },
        'default token does not equal app token' (err, results) {
          assert.ifError(err)
          assert.notEqual(results.user.defaultToken, results.token)
        }
      }
    }
  })).export(module)
