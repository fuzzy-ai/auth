// confirmation-test.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('Confirmation')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Confirmation = require('../lib/confirmation')
          callback(null, Confirmation)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Confirmation) {
        assert.ifError(err)
        assert.isFunction(Confirmation)
      },
      'and we set up the database': {
        topic (Confirmation) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema =
            {Confirmation: Confirmation.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new Confirmation': {
          topic (Confirmation) {
            const props = {
              email: 'fakename@fakemail.test',
              password: 'hope strum aplomb swipe dacca',
              app: 'AAAABBBBCCCCDDDD',
              couponCode: 'CONFIRM1'
            }
            Confirmation.create(props, this.callback)
            return undefined
          },
          'it works' (err, confirm) {
            assert.ifError(err)
            assert.isObject(confirm)
            assert.isString(confirm.email)
            assert.equal('fakename@fakemail.test', confirm.email)
            assert.isUndefined(confirm.password)
            assert.isString(confirm.passwordHash)
            assert.isString(confirm.app)
            assert.equal('AAAABBBBCCCCDDDD', confirm.app)
            assert.isString(confirm.code)
            assert.isString(confirm.couponCode)
            assert.equal('CONFIRM1', confirm.couponCode)
          }
        }
      }
    }}).export(module)
