// api-login-unique-token-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const apiBatch = require('./api-batch')

const env = require('./config')

const EMAIL = 'loginuser@unit.test'
const PASSWORD = 'aloud bole phony feign 94th'

const login = function (email, password, callback) {
  const options = {
    url: 'http://localhost:2342/login',
    json: {
      email,
      password
    },
    headers: {
      authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
    }
  }

  return request.post(options, (err, response, body) => {
    if (err) {
      callback(err)
    } else if (response.statusCode !== 200) {
      const code = response.statusCode
      const message = body != null ? body.message : undefined
      callback(new Error(`Bad status code ${code}: ${message}`))
    } else {
      callback(null, body)
    }
  })
}

const register = (email, password, mock, callback) =>

  async.parallel([
    callback =>
      mock.once('template', (name, paramString) => {
        let params
        try {
          params = JSON.parse(paramString)
        } catch (err) {
          callback(err)
        }
        callback(null, __guard__(params.data != null ? params.data.confirmation : undefined, x => x.code))
      }),
    function (callback) {
      const options = {
        url: 'http://localhost:2342/register',
        json: {
          email,
          password
        },
        headers: {
          authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
        }
      }

      return request.post(options, (err, response, body) => {
        if (err) {
          callback(err)
        } else if (response.statusCode !== 202) {
          const code = response.statusCode
          const message = body != null ? body.message : undefined
          callback(new Error(`Bad status code ${code}: ${message}`))
        } else {
          callback(null, body)
        }
      })
    }

  ], (err, results) => {
    if (err) {
      callback(err)
    } else {
      let [code] = Array.from(results)
      const options = {
        url: 'http://localhost:2342/confirm',
        json: {
          code
        },
        headers: {
          authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
        }
      }
      return request.post(options, (err, response, body) => {
        if (err) {
          callback(err)
        } else if (response.statusCode !== 200) {
          code = response.statusCode
          const message = body != null ? body.message : undefined
          callback(new Error(`Bad status code ${code}: ${message}`))
        } else {
          callback(null)
        }
      })
    }
  })

vows
  .describe('/login returns unique tokens')
  .addBatch(apiBatch({
    'and we register a new user': {
      topic (auth, mock) {
        register(EMAIL, PASSWORD, mock, this.callback)
        return undefined
      },
      'it works' (err, body, code) {
        assert.ifError(err)
      },
      'and we login two different times': {
        topic () {
          async.parallel([
            callback => login(EMAIL, PASSWORD, callback),
            callback => login(EMAIL, PASSWORD, callback)
          ], this.callback)
          return undefined
        },
        'tokens are unique' (err, bodies) {
          assert.ifError(err)
          assert.isArray(bodies)
          assert.isObject(bodies[0])
          assert.isObject(bodies[1])
          assert.notEqual(bodies[0].token, bodies[1].token)
        }
      }
    }
  })).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
