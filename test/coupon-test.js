// coupon-test.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('Coupon')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Coupon = require('../lib/coupon')
          callback(null, Coupon)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Coupon) {
        assert.ifError(err)
        assert.isFunction(Coupon)
      },
      'and we set up the database': {
        topic (Coupon) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema =
            {Coupon: Coupon.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new Coupon': {
          topic (Coupon) {
            const props =
              {code: 'UNITTEST1'}
            Coupon.create(props, this.callback)
            return undefined
          },
          'it works' (err, coupon) {
            assert.ifError(err)
            assert.isObject(coupon)
          },
          'and we examine the coupon': {
            topic (coupon) {
              return coupon
            },
            'it has a code' (err, coupon) {
              assert.ifError(err)
              assert.isString(coupon.code)
              assert.equal('UNITTEST1', coupon.code)
            },
            'it has a createdAt timestamp' (err, coupon) {
              assert.ifError(err)
              assert.isString(coupon.createdAt)
              assert.inDelta(Date.parse(coupon.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, coupon) {
              assert.ifError(err)
              assert.isString(coupon.updatedAt)
              assert.inDelta(Date.parse(coupon.updatedAt), Date.now(), 5000)
            }
          }
        }
      }
    }}).export(module)
