// api-reset-password-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const EMAIL = 'resetpassword@mail.test'
const PASSWORD = '28 prom glamor rt pierce'
const PASSWORD2 = 'size 123 await cider pagan'

vows
  .describe('POST /reset')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we register a new user': {
          topic (auth, mock) {
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/register',
              json: {
                email: EMAIL,
                password: PASSWORD
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }

            async.parallel([
              callback =>
                mock.once('template', (name, paramString) => {
                  let params
                  try {
                    params = JSON.parse(paramString)
                  } catch (err) {
                    callback(err)
                  }
                  callback(null, [name, params])
                }),
              callback =>
                request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 202) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    callback(null, body)
                  }
                })

            ], (err, results) => {
              if (err) {
                callback(err)
              } else {
                const [[name, params], body] = results
                callback(null, body, name, params)
              }
            })
            return undefined
          },
          'it works' (err, body, name, params) {
            assert.ifError(err)
          },
          'and we confirm the registration': {
            topic (body, name, params) {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/confirm',
                json: {
                  code: params.data.confirmation.code
                },
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }
              request.post(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  const code = response.statusCode
                  const message = body != null ? body.message : undefined
                  callback(new Error(`Bad status code ${code}: ${message}`))
                } else {
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, results) {
              assert.ifError(err)
              assert.isObject(results)
              assert.isObject(results.user)
              assert.isString(results.token)
            },
            'and we ask for a password reset': {
              topic (results, body, name, params, auth, mock) {
                const { callback } = this
                const options = {
                  url: 'http://localhost:2342/request-reset',
                  json: {
                    email: EMAIL
                  },
                  headers: {
                    authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                  }
                }
                async.parallel([
                  callback =>
                    mock.once('template', (name, paramString) => {
                      try {
                        params = JSON.parse(paramString)
                      } catch (err) {
                        callback(err)
                      }
                      callback(null, [name, params])
                    }),
                  callback =>
                    request.post(options, (err, response, body) => {
                      if (err) {
                        callback(err)
                      } else if (response.statusCode !== 200) {
                        const code = response.statusCode
                        const message = body != null ? body.message : undefined
                        const emessage = `Bad status code ${code}: ${message}`
                        callback(new Error(emessage))
                      } else {
                        callback(null, body)
                      }
                    })

                ], (err, results) => {
                  if (err) {
                    callback(err)
                  } else {
                    const [[name, params], body] = results
                    callback(null, body, name, params)
                  }
                })
                return undefined
              },
              'it works' (err, body, name, params) {
                assert.ifError(err)
              },
              'body looks correct' (err, body, name, params) {
                assert.ifError(err)
                assert.isObject(body)
                assert.isString(body.status)
                assert.equal('awaiting email confirmation', body.status)
              },
              'mailer was called' (err, body, name, params) {
                assert.ifError(err)
                assert.equal('reset', name)
                assert.isObject(params)
                assert.isObject(params.data)
                assert.isObject(params.data.confirmation)
                assert.isString(params.data.confirmation.code)
              },
              'and we reset the password': {
                topic (body, name, params) {
                  const { callback } = this
                  const options = {
                    url: 'http://localhost:2342/reset',
                    json: {
                      code: params.data.confirmation.code,
                      password: PASSWORD2
                    },
                    headers: {
                      authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                    }
                  }
                  request.post(options, (err, response, body) => {
                    if (err) {
                      callback(err)
                    } else if (response.statusCode !== 200) {
                      const code = response.statusCode
                      const message = body != null ? body.message : undefined
                      const emessage = `Bad status code ${code}: ${message}`
                      callback(new Error(emessage))
                    } else {
                      callback(null, body)
                    }
                  })
                  return undefined
                },
                'it works' (err, body) {
                  assert.ifError(err)
                },
                'body looks correct' (err, body) {
                  assert.ifError(err)
                  assert.isObject(body)
                  assert.isObject(body.user)
                  assert.isString(body.token)
                  assert.notInclude(body.user, 'password')
                  assert.notInclude(body.user, 'passwordHash')
                  assert.isString(body.user.defaultToken)
                },
                'and we log in with the old password': {
                  topic () {
                    const { callback } = this
                    const options = {
                      url: 'http://localhost:2342/login',
                      json: {
                        email: EMAIL,
                        password: PASSWORD
                      },
                      headers: {
                        authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                      }
                    }
                    request.post(options, (err, response, body) => {
                      if (err) {
                        callback(err)
                      } else if (response.statusCode === 200) {
                        callback(new Error('Unexpected success'))
                      } else if (response.statusCode !== 401) {
                        const code = response.statusCode
                        const message = body != null ? body.message : undefined
                        const emessage = `Bad status code ${code}: ${message}`
                        callback(new Error(emessage))
                      } else {
                        callback(null)
                      }
                    })
                    return undefined
                  },
                  'it fails correctly' (err) {
                    assert.ifError(err)
                  }
                },
                'and we log in with the new password': {
                  topic () {
                    const { callback } = this
                    const options = {
                      url: 'http://localhost:2342/login',
                      json: {
                        email: EMAIL,
                        password: PASSWORD2
                      },
                      headers: {
                        authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                      }
                    }
                    request.post(options, (err, response, body) => {
                      if (err) {
                        callback(err)
                      } else if (response.statusCode !== 200) {
                        const code = response.statusCode
                        const message = body != null ? body.message : undefined
                        const emessage = `Bad status code ${code}: ${message}`
                        callback(new Error(emessage))
                      } else {
                        callback(null, body)
                      }
                    })
                    return undefined
                  },
                  'it works' (err, results) {
                    assert.ifError(err)
                  },
                  'it looks right' (err, results) {
                    assert.ifError(err)
                    assert.isObject(results)
                    assert.isObject(results.user)
                    assert.isString(results.token)
                  },
                  'user has an id' (err, results) {
                    assert.ifError(err)
                    assert.isString(results.user.id)
                  },
                  'user has a default token' (err, results) {
                    assert.ifError(err)
                    assert.isString(results.user.defaultToken)
                  },
                  'default token does not equal app token' (err, results) {
                    assert.ifError(err)
                    assert.notEqual(results.user.defaultToken, results.token)
                  },
                  'user has the correct email address' (err, results) {
                    assert.ifError(err)
                    assert.equal(results.user.email, EMAIL)
                  },
                  'user does not have a password property' (err, results) {
                    assert.ifError(err)
                    assert.notInclude(results.user, 'password')
                    assert.notInclude(results.user, 'passwordHash')
                  }
                }
              }
            }
          }
        }
      }
    }}).export(module)
