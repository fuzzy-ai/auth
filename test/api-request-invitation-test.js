// api-request-invitation-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

vows
  .describe('POST /request')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we request an invitation': {
          topic (auth, mock) {
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/request',
              json: {
                email: 'fakeuser@unit.test'
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }

            async.parallel([
              callback =>
                mock.once('template', (name, paramString) => {
                  let params
                  try {
                    params = JSON.parse(paramString)
                  } catch (err) {
                    callback(err)
                  }
                  callback(null, [name, params])
                }),
              callback =>
                request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    callback(null, body)
                  }
                })

            ], (err, results) => {
              if (err) {
                callback(err)
              } else {
                const [[name, params], body] = results
                callback(null, body, name, params)
              }
            })
            return undefined
          },
          'it works' (err, body, name, params) {
            assert.ifError(err)
          },
          'body looks correct' (err, body, name, params) {
            assert.ifError(err)
            assert.isString(body)
            assert.equal('OK', body)
          },
          'mailer was called' (err, body, name, params) {
            assert.ifError(err)
            assert.equal('invitation-request', name)
            assert.isObject(params)
            assert.isString(params.to)
            assert.isString(params.subject)
            assert.isObject(params.data)
            assert.isString(params.data.email)
          },
          'and we try to request again with the same email address': {
            topic () {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/request',
                json: {
                  email: 'fakeuser@unit.test'
                },
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }
              request.post(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 409) {
                  const code = response.statusCode
                  const message = body != null ? body.message : undefined
                  callback(new Error(`Bad status code ${code}: ${message}`))
                } else {
                  callback(null)
                }
              })
              return undefined
            },
            'it fails correctly' (err) {
              assert.ifError(err)
            }
          }
        }
      }
    }}).export(module)
