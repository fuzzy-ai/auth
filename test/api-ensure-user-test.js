// api-ensure-user-test.js
// Copyright 2015 Evan Prodromou <evan@prodromou.name>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const _ = require('lodash')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const EMAIL = 'ensure-user@unit.test'
const PASSWORD = 'hoi luger 11th torus ey'

vows
  .describe('ENSURE_USER environment variable')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const extra = {'ENSURE_USER': `${EMAIL}:${PASSWORD}`}
            const server = new AuthServer(_.assign({}, env, extra))
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we log in the ensured user': {
          topic () {
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/login',
              json: {
                email: EMAIL,
                password: PASSWORD
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }
            request.post(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 200) {
                const code = response.statusCode
                const message = body != null ? body.message : undefined
                callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                callback(null, body)
              }
            })
            return undefined
          },
          'it works' (err, results) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)
