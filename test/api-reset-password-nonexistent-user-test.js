// api-reset-password-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')

const apiBatch = require('./api-batch')

const env = require('./config')

const EMAIL = 'doesnotexist@mail.test'

vows
  .describe('POST /reset for nonexistent user')
  .addBatch(apiBatch({
    'and we ask for a password reset for a nonexistent user': {
      topic (results, body, name, params, auth, mock) {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/request-reset',
          json: {
            email: EMAIL
          },
          headers: {
            authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            callback(err)
          } else if (response.statusCode !== 200) {
            const code = response.statusCode
            const message = body != null ? body.message : undefined
            const emessage = `Bad status code ${code}: ${message}`
            callback(new Error(emessage))
          } else {
            callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, body) {
        assert.ifError(err)
      },
      'body looks correct' (err, body, name, params) {
        assert.ifError(err)
        assert.isObject(body)
        assert.isString(body.status)
        assert.equal('awaiting email confirmation', body.status)
      }
    }
  })).export(module)
