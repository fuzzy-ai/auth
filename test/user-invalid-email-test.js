// user-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('User with invalid email')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const User = require('../lib/user')
          callback(null, User)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, User) {
        assert.ifError(err)
        assert.isFunction(User)
      },
      'and we set up the database': {
        topic (User) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema = {user: User.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new user': {
          topic (User) {
            User.create({email: 'fakename@mail.localhost'}, this.callback)
            return undefined
          },
          'it works' (err, user) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (user) {
              return user
            },
            'it has the passed-in email address' (err, user) {
              assert.ifError(err)
              assert.equal(user.email, 'fakename@mail.localhost')
            },
            'it has an ID' (err, user) {
              assert.ifError(err)
              assert.isString(user.id)
            },
            'it has a createdAt timestamp' (err, user) {
              assert.ifError(err)
              assert.isString(user.createdAt)
              assert.inDelta(Date.parse(user.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, user) {
              assert.ifError(err)
              assert.isString(user.updatedAt)
              assert.inDelta(Date.parse(user.updatedAt), Date.now(), 5000)
            }
          },
          'and we wait a couple of seconds': {
            topic (user) {
              const { callback } = this
              const wait = () => callback(null)
              setTimeout(wait, 2000)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the user object with an invalid email address': {
              topic (user) {
                user.update({email: 'fakename@othermail.localhost<script>XSS</script>'}, (err, updated) => {
                  if (!err) {
                    this.callback(new Error('Unexpected success'))
                  } else {
                    this.callback(null, err)
                  }
                })
                return undefined
              },
              'it throws an error' (err, reported) {
                assert.ifError(err)
                assert.isObject(reported)
                assert.equal(reported.name, 'InvalidEmailError')
              }
            }
          }
        }
      }
    }}).export(module)
