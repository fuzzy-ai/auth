// accesstoken-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

const Application = require('../lib/application')

vows
  .describe('AccessToken')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const AccessToken = require('../lib/accesstoken')
          callback(null, AccessToken)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, AccessToken) {
        assert.ifError(err)
        assert.isFunction(AccessToken)
      },
      'and we set up the database': {
        topic (AccessToken) {
          const { callback } = this
          const params = env.PARAMS
          params.schema = {
            accesstoken: AccessToken.schema,
            application: Application.schema
          }
          const db = Databank.get(env.DRIVER, params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new Application': {
          topic () {
            const props = {
              name: 'Unit test',
              url: 'https://unit.test/'
            }
            Application.create(props, this.callback)
            return undefined
          },
          'it works' (err, app) {
            assert.ifError(err)
            assert.isObject(app)
          },
          'and we create a new AccessToken': {
            topic (app, AccessToken) {
              AccessToken.create({userID: 'aaaabbbbcc', app: app.id}, this.callback)
              return undefined
            },
            'it works' (err, token) {
              assert.ifError(err)
            },
            'and we examine it': {
              topic (token) {
                return token
              },
              'it has the passed-in user ID' (err, token) {
                assert.ifError(err)
                assert.equal(token.userID, 'aaaabbbbcc')
              },
              'it has a token property' (err, token) {
                assert.ifError(err)
                assert.isString(token.token)
              },
              'it has a createdAt timestamp' (err, token) {
                assert.ifError(err)
                assert.isString(token.createdAt)
                assert.inDelta(Date.parse(token.createdAt), Date.now(), 5000)
              },
              'it has an updatedAt timestamp' (err, token) {
                assert.ifError(err)
                assert.isString(token.updatedAt)
                assert.inDelta(Date.parse(token.updatedAt), Date.now(), 5000)
              }
            },
            'and we compare with the app': {
              topic (token, app) {
                return [token, app]
              },
              'they match' (parts) {
                const [token, app] = Array.from(parts)
                assert.equal(token.app, app.id)
              }
            }
          }
        },
        'and we create a new AccessToken with no application': {
          topic (AccessToken) {
            AccessToken.create({userID: 'aaaabbbbcc'}, this.callback)
            return undefined
          },
          'it works' (err, token) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (token) {
              return token
            },
            'it has the passed-in user ID' (err, token) {
              assert.ifError(err)
              assert.equal(token.userID, 'aaaabbbbcc')
            },
            'it has a token property' (err, token) {
              assert.ifError(err)
              assert.isString(token.token)
            },
            'it has a createdAt timestamp' (err, token) {
              assert.ifError(err)
              assert.isString(token.createdAt)
              assert.inDelta(Date.parse(token.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, token) {
              assert.ifError(err)
              assert.isString(token.updatedAt)
              assert.inDelta(Date.parse(token.updatedAt), Date.now(), 5000)
            }
          },
          'and we get the user\'s default token': {
            topic (token, AccessToken) {
              const { callback } = this
              AccessToken.default('aaaabbbbcc', (err, dt) => {
                if (err) {
                  callback(err)
                } else {
                  callback(null, token, dt)
                }
              })
              return undefined
            },
            'they are the same' (err, token, dt) {
              assert.ifError(err)
              assert.isObject(token)
              assert.isObject(dt)
              assert.equal(token.token, dt.token)
            }
          }
        }
      }
    }}).export(module)
