// mailer-mock.js
// Copyright 2014 fuzzy.ai <evan@fuzzy.ai>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

const http = require('http')
const events = require('events')
const debug = require('debug')('auth:mailer-mock')
const uuid = require('uuid')

const JSON_TYPE = 'application/json; charset=utf-8'

class MailerServerMock extends events.EventEmitter {
  constructor (code) {
    super()
    const self = this
    const server = http.createServer((request, response) => {
      let body = ''
      const respond = function (code, body) {
        response.statusCode = code
        if (!response.headersSent) {
          response.setHeader('Content-Type', JSON_TYPE)
        }
        return response.end(JSON.stringify(body))
      }
      request.on('data', chunk => { body += chunk })
      request.on('error', err => respond(500, {status: 'error', message: err.message}))
      return request.on('end', () => {
        const auth = request.headers.authorization
        debug(`auth = ${auth}`)
        debug(`code = ${code}`)
        if (!auth || (auth !== `Bearer ${code}`)) {
          respond(403, {status: 'error', message: 'No access'})
        }
        if (request.method === 'POST') {
          const name = request.url.slice(1)
          self.emit('template', name, body)
          return respond(202, {status: 'Accepted', id: uuid.v4()})
        } else if ((request.method === 'GET') && (request.url === '/version')) {
          return respond(200, {version: '1.0.0'})
        } else {
          // If we get here, no route found
          const verb = request.method
          const { url } = request
          return respond(404, {status: 'error', message: `Cannot ${verb} ${url}`})
        }
      })
    })

    this.start = function (callback) {
      server.once('error', err => callback(err))
      server.once('listening', () => callback(null))
      return server.listen(1516)
    }

    this.stop = function (callback) {
      server.once('close', () => callback(null))
      server.once('error', err => callback(err))
      return server.close()
    }
  }
}

module.exports = MailerServerMock
