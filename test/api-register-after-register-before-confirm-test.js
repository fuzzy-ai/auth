// api-register-after-register-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const async = require('async')
const request = require('request')

const apiBatch = require('./api-batch')
const env = require('./config')

const EMAIL = 'fakeuser@unit.test'
const PASSWORD = 'ease wrack strap girl bcd'

vows
  .describe('POST /register for unconfirmed user')
  .addBatch(apiBatch({
    'and we register a new user': {
      topic (auth, mock) {
        const { callback } = this
        const options = {
          url: 'http://localhost:2342/register',
          json: {
            email: EMAIL,
            password: PASSWORD
          },
          headers: {
            authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
          }
        }
        request.post(options, (err, response, body) => {
          if (err) {
            callback(err)
          } else if (response.statusCode !== 202) {
            const code = response != null ? response.statusCode : undefined
            const msg = body != null ? body.message : undefined
            callback(new Error(`Bad status code ${code}: ${msg}`))
          } else {
            callback(null, body)
          }
        })
        return undefined
      },
      'it works' (err, body) {
        assert.ifError(err)
      },
      'and we try to register again': {
        topic (body, auth, mock) {
          const { callback } = this
          const options = {
            url: 'http://localhost:2342/register',
            json: {
              email: EMAIL,
              password: PASSWORD
            },
            headers: {
              authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
            }
          }

          async.parallel([
            callback =>
              mock.once('template', (name, paramString) => {
                let params
                try {
                  params = JSON.parse(paramString)
                } catch (err) {
                  callback(err)
                }
                callback(null, [name, params])
              }),
            callback =>
              request.post(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 202) {
                  const code = response.statusCode
                  const message = body != null ? body.message : undefined
                  callback(new Error(`Bad status code ${code}: ${message}`))
                } else {
                  callback(null, body)
                }
              })

          ], (err, results) => {
            if (err) {
              callback(err)
            } else {
              const [[name, params], body] = results
              callback(null, body, name, params)
            }
          })
          return undefined
        },
        'it works' (err, body, name, params) {
          assert.ifError(err)
        },
        'body looks correct' (err, body, name, params) {
          assert.ifError(err)
          assert.isObject(body)
          assert.isString(body.status)
          assert.equal('awaiting email confirmation', body.status)
        },
        'mailer was called' (err, body, name, params) {
          assert.ifError(err)
          assert.equal('confirmation', name)
          assert.isObject(params)
          assert.isObject(params.data)
          assert.isObject(params.data.confirmation)
          assert.isString(params.data.confirmation.code)
        }
      }
    }
  })).export(module)
