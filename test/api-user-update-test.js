// api-user-update-test.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

vows
  .describe('PUT /user/:userID')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we register a new user': {
          topic (auth, mock) {
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/register',
              json: {
                email: 'logoutuser@unit.test',
                password: 'cicada flam ek warty mercy'
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }

            async.parallel([
              callback =>
                mock.once('template', (name, paramString) => {
                  let params
                  try {
                    params = JSON.parse(paramString)
                  } catch (err) {
                    callback(err)
                  }
                  callback(null, __guard__(params.data != null ? params.data.confirmation : undefined, x => x.code))
                }),
              callback =>
                request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 202) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    const emessage = `Bad status code ${code}: ${message}`
                    callback(new Error(emessage))
                  } else {
                    callback(null, body)
                  }
                })

            ], (err, results) => {
              if (err) {
                callback(err)
              } else {
                const [code, body] = Array.from(results)
                callback(null, body, code)
              }
            })
            return undefined
          },
          'it works' (err, body, code) {
            assert.ifError(err)
          },
          'and we confirm the registration': {
            topic (body, code) {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/confirm',
                json: {
                  code
                },
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }
              request.post(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  code = response.statusCode
                  const message = body != null ? body.message : undefined
                  const emessage = `Bad status code ${code}: ${message}`
                  callback(new Error(emessage))
                } else {
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, results) {
              assert.ifError(err)
            },
            'and we update the user': {
              topic (results, body, code) {
                const { callback } = this
                const options = {
                  url: `http://localhost:2342/user/${results.user.id}`,
                  json: {
                    email: 'newemail@unit.test'
                  },
                  headers: {
                    authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                  }
                }
                request.put(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    code = response.statusCode
                    const message = body != null ? body.message : undefined
                    const emessage = `Bad status code ${code}: ${message}`
                    callback(new Error(emessage))
                  } else {
                    callback(null, body)
                  }
                })
                return undefined
              },
              'it works' (err, results) {
                assert.ifError(err)
                assert.isObject(results)
                assert.isString(results.status)
                assert.equal(results.status, 'OK')
                assert.isObject(results.user)
                assert.equal(results.user.email, 'newemail@unit.test')
              }
            }
          }
        }
      }
    }}).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
