// api-register-with-subscribe-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')
const debug = require('debug')('auth:api-register-with-subscribe-test')

const MailerServerMock = require('./mailer-mock')
const SendgridServerMock = require('./sendgrid-mock')
const env = require('./config')

vows
  .describe('POST /register with subscribe')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start a mock Sendgrid server': {
        topic (mock) {
          const { callback } = this
          const srv = new SendgridServerMock(env.SENDGRID_KEY)
          srv.start(err => callback(err, srv))
          return undefined
        },
        'it works' (err, sg) {
          assert.ifError(err)
          assert.isObject(sg)
        },
        'teardown' (sg) {
          sg.stop(this.callback)
        },
        'and we start an AuthServer': {
          topic (sg, mock) {
            const { callback } = this
            try {
              const AuthServer = require('../lib/authserver')
              const server = new AuthServer(env)
              server.start((err) => {
                if (err) {
                  callback(err, null)
                } else {
                  callback(null, server)
                }
              })
            } catch (error) {
              const err = error
              callback(err)
            }
            return undefined
          },
          'it works' (err, server) {
            assert.ifError(err)
          },
          'teardown' (server) {
            server.stop(this.callback)
          },
          'and we register a new user': {
            topic (auth, sg, mock) {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/register',
                json: {
                  email: 'fakeuser@unit.test',
                  password: 'aloud bole phony feign 94th',
                  subscribe: true
                },
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }

              async.parallel([
                callback =>
                  mock.once('template', (name, paramString) => {
                    let params
                    try {
                      params = JSON.parse(paramString)
                    } catch (err) {
                      callback(err)
                    }
                    callback(null, [name, params])
                  }),
                callback =>
                  request.post(options, (err, response, body) => {
                    if (err) {
                      callback(err)
                    } else if (response.statusCode !== 202) {
                      const code = response.statusCode
                      const message = body != null ? body.message : undefined
                      callback(new Error(`Bad status code ${code}: ${message}`))
                    } else {
                      callback(null, body)
                    }
                  })

              ], (err, results) => {
                if (err) {
                  callback(err)
                } else {
                  const [[name, params], body] = results
                  callback(null, body, name, params)
                }
              })
              return undefined
            },
            'it works' (err, body, name, params) {
              assert.ifError(err)
              assert.isString(__guard__(__guard__(params != null ? params.data : undefined, x1 => x1.confirmation), x => x.code))
            },
            'and we confirm the registration': {
              topic (body, name, params, auth, sg, mock) {
                const { callback } = this
                const options = {
                  url: 'http://localhost:2342/confirm',
                  json: {
                    code: params.data.confirmation.code
                  },
                  headers: {
                    authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                  }
                }
                const urls = []
                const addURL = function (req, res) {
                  debug(`Request to url: ${req.url}`)
                  return urls.push(req.url)
                }
                sg.on('request', addURL)
                request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    const code = response.statusCode
                    const msg = body != null ? body.message : undefined
                    callback(new Error(`Unexpected status code ${code}: ${msg}`))
                  } else {
                    callback(null, body, urls)
                  }
                })
                return undefined
              },
              'it works' (err, body, urls) {
                assert.ifError(err)
              },
              'Sendgrid API is called' (err, body, urls) {
                assert.ifError(err)
                const url = '/v3/contactdb/lists/1/recipients'
                assert.notEqual(urls.indexOf(url), -1)
              }
            }
          }
        }
      }
    }}).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
