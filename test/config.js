// config.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const env = {
  PORT: '2342',
  DRIVER: 'memory',
  PARAMS: '{}',
  LOG_FILE: '/dev/null',
  AUTH_PRIVILEGED_KEY: 'olive-poppy-viola-dupe-sliver',
  AUTH_SUPPORT_EMAIL: 'fake@fuzzyio.test',
  SENDGRID_SERVER: 'http://localhost:4815',
  SENDGRID_KEY: 'monk-sedge-spit-hogar-give',
  SUBSCRIBER_LIST: 'unit test',
  AUTH_MAILER_SERVER: 'http://localhost:1516',
  AUTH_MAILER_KEY: 'test-app-key',
  ENSURE_USER: 'ensure-config@unit.test:redeem scoop poi iw blast',
  WEB_CLIENT_TIMEOUT: '0'
}

module.exports = env
