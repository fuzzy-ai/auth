// api-register-with-code-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const Coupon = require('../lib/coupon')
const Redemption = require('../lib/redemption')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const EMAIL = 'userwithcouponcode@unit.test'
const PASSWORD = 'cave wavy waive wyman blood'
const CODE = 'REGISTERTEST1'

vows
  .describe('POST /register with coupon code')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we create a coupon code': {
          topic (auth, mock) {
            // This works because we're in the same process
            // as the auth server and it's using a memory databank
            Coupon.create({code: CODE}, this.callback)
            return undefined
          },
          'it works' (err, coupon) {
            assert.ifError(err)
            assert.isObject(coupon)
            assert.isString(coupon.code)
          },
          'and we register a new user with that code': {
            topic (coupon, auth, mock) {
              const { callback } = this
              const options = {
                url: 'http://localhost:2342/register',
                json: {
                  email: EMAIL,
                  password: PASSWORD,
                  couponCode: coupon.code
                },
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }
              async.parallel([
                callback =>
                  mock.once('template', (name, paramString) => {
                    let params
                    try {
                      params = JSON.parse(paramString)
                    } catch (err) {
                      callback(err)
                    }
                    callback(null, [name, params])
                  }),
                callback =>
                  request.post(options, (err, response, body) => {
                    if (err) {
                      callback(err)
                    } else if (response.statusCode !== 202) {
                      const code = response.statusCode
                      const message = body != null ? body.message : undefined
                      callback(new Error(`Bad status code ${code}: ${message}`))
                    } else {
                      callback(null, body)
                    }
                  })

              ], (err, results) => {
                if (err) {
                  callback(err)
                } else {
                  const [[name, params], body] = results

                  callback(null, body, name, params)
                }
              })
              return undefined
            },
            'it works' (err, body, name, params) {
              assert.ifError(err)
            },
            'body looks correct' (err, body, name, params) {
              assert.ifError(err)
              assert.isObject(body)
              assert.isString(body.status)
              assert.equal('awaiting email confirmation', body.status)
            },
            'mailer was called' (err, body, name, params) {
              assert.ifError(err)
              assert.equal('confirmation', name)
              assert.isObject(params)
              assert.isObject(params.data)
              assert.isObject(params.data.confirmation)
              assert.isString(params.data.confirmation.code)
            },
            'and we confirm the registration': {
              topic (body, name, params) {
                const { callback } = this
                const options = {
                  url: 'http://localhost:2342/confirm',
                  json: {
                    code: params.data.confirmation.code
                  },
                  headers: {
                    authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                  }
                }
                request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 200) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    callback(null, body)
                  }
                })
                return undefined
              },
              'it works' (err, results) {
                assert.ifError(err)
                assert.isObject(results)
                assert.isObject(results.user)
              },
              'and we look for a matching coupon redemption': {
                topic (results) {
                  const id = Redemption.makeID({userID: results.user.id, code: CODE})
                  // This works because we're in the same process
                  // as the auth server and it's using a memory databank
                  Redemption.get(id, this.callback)
                  return undefined
                },
                'it works' (err, redemption) {
                  assert.ifError(err)
                  assert.isObject(redemption)
                }
              }
            }
          }
        }
      }
    }}).export(module)
