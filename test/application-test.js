// application-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank

const env = require('./config')

vows
  .describe('Application')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Application = require('../lib/application')
          callback(null, Application)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Application) {
        assert.ifError(err)
        assert.isFunction(Application)
      },
      'and we set up the database': {
        topic (Application) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema = {Application: Application.schema}
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new Application': {
          topic (Application) {
            const props = {
              name: 'My Application',
              url: 'http://myapp.example/'
            }
            Application.create(props, this.callback)
            return undefined
          },
          'it works' (err, app) {
            assert.ifError(err)
          },
          'and we examine it': {
            topic (app) {
              return app
            },
            'it has an auto-generated id' (err, app) {
              assert.ifError(err)
              assert.isString(app.id)
            },
            'it has the passed-in name' (err, app) {
              assert.ifError(err)
              assert.equal(app.name, 'My Application')
            },
            'it has the passed-in url' (err, app) {
              assert.ifError(err)
              assert.equal(app.url, 'http://myapp.example/')
            },
            'it has a createdAt timestamp' (err, app) {
              assert.ifError(err)
              assert.isString(app.createdAt)
              assert.inDelta(Date.parse(app.createdAt), Date.now(), 5000)
            },
            'it has an updatedAt timestamp' (err, app) {
              assert.ifError(err)
              assert.isString(app.updatedAt)
              assert.inDelta(Date.parse(app.updatedAt), Date.now(), 5000)
            }
          },
          'and we wait a couple of seconds': {
            topic (app) {
              const { callback } = this
              const wait = () => callback(null)
              setTimeout(wait, 2000)
              return undefined
            },
            'it works' (err) {
              assert.ifError(err)
            },
            'and we update the Application object': {
              topic (app) {
                app.update({name: 'My Application II'}, this.callback)
                return undefined
              },
              'it works' (err, app) {
                assert.ifError(err)
                assert.isObject(app)
                assert.equal(app.name, 'My Application II')
              },
              'its update timestamp is updated' (err, app) {
                assert.ifError(err)
                assert.notEqual(app.updatedAt, app.createdAt)
                assert.inDelta(Date.parse(app.updatedAt), Date.now(), 5000)
              }
            }
          }
        }
      }
    }}).export(module)
