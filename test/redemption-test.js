// redemption-test.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const databank = require('databank')
const { Databank } = databank
const { DatabankObject } = databank
const async = require('async')

const Coupon = require('../lib/coupon')
const User = require('../lib/user')

const env = require('./config')

vows
  .describe('Redemption')
  .addBatch({
    'When we load the module': {
      topic () {
        const { callback } = this
        try {
          const Redemption = require('../lib/redemption')
          callback(null, Redemption)
        } catch (err) {
          callback(err, null)
        }
        return undefined
      },
      'it works' (err, Redemption) {
        assert.ifError(err)
        assert.isFunction(Redemption)
      },
      'and we set up the database': {
        topic (Redemption) {
          const { callback } = this
          const params = env['PARAMS']
          params.schema = {
            Redemption: Redemption.schema,
            Coupon: Coupon.schema,
            User: User.schema
          }
          const db = Databank.get(env['DRIVER'], params)
          db.connect({}, (err) => {
            if (err) {
              callback(err)
            } else {
              DatabankObject.bank = db
              callback(null)
            }
          })
          return undefined
        },
        'it works' (err) {
          assert.ifError(err)
        },
        'and we create a new Redemption': {
          topic (Redemption) {
            const { callback } = this
            let user = null
            let coupon = null
            async.waterfall([
              callback => User.create({email: 'fakename@mail.localhost'}, callback),
              function (results, callback) {
                user = results
                return Coupon.create({code: 'UNITTEST2'}, callback)
              },
              function (results, callback) {
                coupon = results
                const props = {
                  userID: user.id,
                  code: coupon.code,
                  context: 'unittest'
                }
                return Redemption.create(props, callback)
              }
            ], callback)
            return undefined
          },
          'it works' (err, redemption) {
            assert.ifError(err)
            assert.isObject(redemption)
          },
          'and we examine the redemption': {
            topic (redemption) {
              return redemption
            },
            'it has a user ID' (err, redemption) {
              assert.ifError(err)
              assert.isString(redemption.userID)
            },
            'it has a coupon code' (err, redemption) {
              assert.ifError(err)
              assert.isString(redemption.code)
              assert.equal('UNITTEST2', redemption.code)
            },
            'it has a context' (err, redemption) {
              assert.ifError(err)
              assert.isString(redemption.context)
              assert.equal('unittest', redemption.context)
            },
            'it has a createdAt timestamp' (err, redemption) {
              assert.ifError(err)
              assert.isString(redemption.createdAt)
              assert.inDelta(Date.parse(redemption.createdAt), Date.now(), 5000)
            }
          }
        }
      }
    }}).export(module)
