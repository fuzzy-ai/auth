// api-batch.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows

const _ = require('lodash')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const apiBatch = function (rest) {
  const base = {
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock(env.AUTH_MAILER_KEY)
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        }
      }
    }
  }

  const a = 'When we start a mock mailer server'
  const b = 'and we start an AuthServer'

  _.assign(base[a][b], rest)

  return base
}

module.exports = apiBatch
