// api-validate-coupon-code-test.js
// Copyright 2015 Evan Prodromou <evan@prodromou.name>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')

const Coupon = require('../lib/coupon')

const MailerServerMock = require('./mailer-mock')

const env = require('./config')

const INVALIDCODE = 'INVALIDCOUPON2015'
const VALIDCODE = 'VALIDCOUPON2015'

vows
  .describe('GET /coupon/:couponCode')
  .addBatch({
    'When we start a mock mailer server': {
      topic () {
        const { callback } = this
        const mock = new MailerServerMock('testappkey')
        mock.start(err => callback(err, mock))
        return undefined
      },
      'it works' (err, mock) {
        assert.ifError(err)
        assert.isObject(mock)
      },
      'teardown' (mock) {
        mock.stop(this.callback)
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const server = new AuthServer(env)
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we create a coupon': {
          topic (auth, mock) {
            // This works because we're in the same process
            // as the auth server and it's using a memory databank
            Coupon.create({code: VALIDCODE}, this.callback)
            return undefined
          },
          'it works' (err, coupon) {
            assert.ifError(err)
            assert.isObject(coupon)
            assert.isString(coupon.code)
          },
          'and we fetch the coupon': {
            topic () {
              const { callback } = this
              const options = {
                url: `http://localhost:2342/coupon/${VALIDCODE}`,
                json: true,
                headers: {
                  authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                }
              }
              request.get(options, (err, response, body) => {
                if (err) {
                  callback(err)
                } else if (response.statusCode !== 200) {
                  const code = response.statusCode
                  const message = body != null ? body.message : undefined
                  const emessage = `Bad status code ${code}: ${message}`
                  callback(new Error(emessage))
                } else {
                  callback(null, body)
                }
              })
              return undefined
            },
            'it works' (err, body) {
              assert.ifError(err)
              assert.isObject(body)
              assert.equal(body.code, VALIDCODE)
            }
          }
        },
        'and we fetch an invalid coupon code': {
          topic () {
            const { callback } = this
            const options = {
              url: `http://localhost:2342/coupon/${INVALIDCODE}`,
              json: true,
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }
            request.get(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 404) {
                const code = response.statusCode
                const message = body != null ? body.message : undefined
                const emessage = `Bad status code ${code}: ${message}`
                callback(new Error(emessage))
              } else {
                callback(null)
              }
            })
            return undefined
          },
          'it fails correctly' (err) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)
