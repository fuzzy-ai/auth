// api-confirm-after-confirm-test.js
// Copyright 2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const apiBatch = require('./api-batch')
const env = require('./config')

const EMAIL = 'confirm-after-confirm@unit.test'
const PASSWORD = 'aloud bole phony feign 94th'

vows
  .describe('POST /confirm for already confirmed user')
  .addBatch(apiBatch({
    'and we register and confirm a user': {
      topic (auth, mock) {
        const { callback } = this
        async.waterfall([
          callback =>
            // Register the user and get a code
            async.parallel([
              callback =>
                mock.once('template', (name, paramString) => {
                  let params
                  try {
                    params = JSON.parse(paramString)
                  } catch (err) {
                    callback(err)
                  }
                  callback(null, [name, params])
                }),
              function (callback) {
                const options = {
                  url: 'http://localhost:2342/register',
                  json: {
                    email: EMAIL,
                    password: PASSWORD
                  },
                  headers: {
                    authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
                  }
                }
                return request.post(options, (err, response, body) => {
                  if (err) {
                    callback(err)
                  } else if (response.statusCode !== 202) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    callback(null, body)
                  }
                })
              }
            ], (err, results) => {
              if (err) {
                callback(err)
              } else {
                const [ [, params] ] = results
                callback(null, __guard__(__guard__(params != null ? params.data : undefined, x1 => x1.confirmation), x => x.code))
              }
            }),
          function (confirmationCode, callback) {
            const options = {
              url: 'http://localhost:2342/confirm',
              json: {
                code: confirmationCode
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }
            return request.post(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 200) {
                const code = response.statusCode
                const message = body != null ? body.message : undefined
                callback(new Error(`Bad status code ${code}: ${message}`))
              } else {
                callback(null, confirmationCode)
              }
            })
          }
        ], (err, confirmationCode) => {
          if (err) {
            callback(err)
          } else {
            callback(null, confirmationCode)
          }
        })
        return undefined
      },
      'it works' (err, confirmationCode) {
        assert.ifError(err)
        assert.isString(confirmationCode)
      },
      'and we try to confirm again with the same code': {
        topic (confirmationCode) {
          const { callback } = this
          const options = {
            url: 'http://localhost:2342/confirm',
            json: {
              code: confirmationCode
            },
            headers: {
              authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
            }
          }
          request.post(options, (err, response, body) => {
            if (err) {
              callback(err)
            } else if (response.statusCode === 200) {
              callback(new Error('Unexpected success'))
            } else if (response.statusCode === 409) {
              callback(null)
            } else {
              const code = response.statusCode
              const message = body != null ? body.message : undefined
              callback(new Error(`Bad status code ${code}: ${message}`))
            }
          })
          return undefined
        },
        'it fails correctly' (err) {
          assert.ifError(err)
        }
      }
    }
  })).export(module)

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
