// api-register-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')

const apiBatch = require('./api-batch')

const env = require('./config')

vows
  .describe('POST /register for existent user')
  .addBatch(apiBatch({
    'and we try to do a registration for an existent user': {
      topic (auth, mock) {
        const { callback } = this
        const [email, password] = Array.from(env.ENSURE_USER.split(':'))
        const options = {
          url: 'http://localhost:2342/register',
          json: {
            email,
            password
          },
          headers: {
            authorization: `Bearer ${env.AUTH_PRIVILEGED_KEY}`
          }
        }
        async.parallel([
          callback =>
            mock.once('template', (name, paramString) => {
              let params
              try {
                params = JSON.parse(paramString)
              } catch (err) {
                callback(err)
              }
              callback(null, [name, params])
            }),
          callback =>
            request.post(options, (err, response, body) => {
              if (err) {
                callback(err)
              } else if (response.statusCode !== 200) {
                const code = response != null ? response.statusCode : undefined
                const msg = body != null ? body.message : undefined
                callback(new Error(`Bad status code ${code}: ${msg}`))
              } else {
                callback(null, body)
              }
            })

        ], (err, results) => {
          if (err) {
            callback(err)
          } else {
            const [[name, params], body] = results
            callback(null, name, params, body)
          }
        })
        return undefined
      },
      'it works' (err, name, params, body) {
        assert.ifError(err)
        assert.isString(name)
        assert.isObject(params)
        assert.isObject(body)
        assert.equal(body.status, 'awaiting email confirmation')
        assert.equal(name, 'no-need-to-register')
      }
    }
  })).export(module)
