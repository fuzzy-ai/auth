// api-error-slack-test.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const http = require('http')

const vows = require('vows')
const { assert } = vows
const request = require('request')
const async = require('async')
const _ = require('lodash')

const env = require('./config')

vows
  .describe('Post to Slack on error')
  .addBatch({
    'When we start a mock slack server': {
      topic () {
        const { callback } = this
        const slack = http.createServer((req, res) => {
          res.writeHead(200, {
            'Content-Type': 'text/plain',
            'Content-Length': '0'
          }
          )
          return res.end()
        })
        slack.listen(1516, () => callback(null, slack))
        return undefined
      },
      'it works' (err, slack) {
        assert.ifError(err)
      },
      'teardown' (slack) {
        const { callback } = this
        slack.once('close', () => callback(null))
        slack.close()
        return undefined
      },
      'and we start an AuthServer': {
        topic (mock) {
          const { callback } = this
          try {
            const AuthServer = require('../lib/authserver')
            const extra = { SLACK_HOOK: 'http://localhost:1516/post-message' }
            const server = new AuthServer(_.assign({}, env, extra))
            server.start((err) => {
              if (err) {
                callback(err, null)
              } else {
                callback(null, server)
              }
            })
          } catch (error) {
            const err = error
            callback(err)
          }
          return undefined
        },
        'it works' (err, server) {
          assert.ifError(err)
        },
        'teardown' (server) {
          server.stop(this.callback)
        },
        'and we request an invitation': {
          topic (auth, slack) {
            // Inject an error into the invite-request code
            const InvitationRequest = require('../lib/invitation-request')
            const oldBC = InvitationRequest.beforeCreate
            InvitationRequest.beforeCreate = (props, callback) => callback(new Error('Injected error from unit test'))
            const { callback } = this
            const options = {
              url: 'http://localhost:2342/request',
              json: {
                email: 'fakeuser@unit.test'
              },
              headers: {
                authorization: `Bearer ${env['AUTH_PRIVILEGED_KEY']}`
              }
            }

            async.parallel([
              callback =>
                slack.once('request', (req, res) => callback(null)),
              callback =>
                request.post(options, (err, response, body) => {
                  InvitationRequest.beforeCreate = oldBC
                  if (err) {
                    callback(err)
                  } else if (response.statusCode === 200) {
                    callback(new Error('Unexpected success'))
                  } else if (response.statusCode !== 500) {
                    const code = response.statusCode
                    const message = body != null ? body.message : undefined
                    callback(new Error(`Bad status code ${code}: ${message}`))
                  } else {
                    callback(null)
                  }
                })

            ], (err, results) => {
              if (err) {
                callback(err)
              } else {
                callback(null)
              }
            })
            return undefined
          },
          'it works' (err) {
            assert.ifError(err)
          },
          'slack was called' (err) {
            assert.ifError(err)
          }
        }
      }
    }}).export(module)
