// authserver.coffee
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const Microservice = require('@fuzzy-ai/microservice')

const bcrypt = require('bcryptjs')

const _ = require('lodash')
const async = require('async')
const validator = require('validator')
const debug = require('debug')('auth:authserver')
const {WebClient} = require('fuzzy.ai-web')

const version = require('./version')
const User = require('./user')
const AccessToken = require('./accesstoken')
const Application = require('./application')
const Confirmation = require('./confirmation')
const Coupon = require('./coupon')
const Redemption = require('./redemption')
const InvitationRequest = require('./invitation-request')
const ResetConfirmation = require('./reset-confirmation')
const HTTPError = require('./httperror')
const MailerClient = require('./mailerclient')
const PasswordError = require('./password-error')
const SendgridClient = require('./sendgrid-client')
const timestamp = require('./timestamp')

class UserExistsError extends Error {
  constructor (email, user) {
    super(`A user with email address ${email} already exists`)
    this.email = email
    this.user = user
    this.name = 'UserExistsError'
  }
}

const appIsPrivileged = function (req, res, next) {
  if (!req.clientApp) {
    return next(new HTTPError('No app', 403))
  } else if (!req.clientApp.privileged) {
    return next(new HTTPError(`${req.clientApp.id} not privileged.`, 403))
  } else {
    return next()
  }
}

class AuthServer extends Microservice {
  environmentToConfig (env) {
    const cfg = super.environmentToConfig(env)
    cfg.privileged = env['AUTH_PRIVILEGED_KEY']
    cfg.mailerServer = env['AUTH_MAILER_SERVER']
    cfg.mailerKey = env['AUTH_MAILER_KEY']
    cfg.couponCodeOnly = env['AUTH_COUPON_CODE_ONLY']
    cfg.supportEmail = env['AUTH_SUPPORT_EMAIL']
    cfg.ensureCoupon = env['ENSURE_COUPON']
    cfg.ensureUser = env['ENSURE_USER']
    cfg.sendgridServer = env['SENDGRID_SERVER'] || 'https://api.sendgrid.com'
    cfg.sendgridKey = env['SENDGRID_KEY']
    cfg.subscriberList = env['SUBSCRIBER_LIST']
    cfg.webClientTimeout = this.envInt(env, 'WEB_CLIENT_TIMEOUT', Infinity)
    debug(cfg)
    return cfg
  }

  setupExpress () {
    const exp = super.setupExpress()
    exp.webClient = new WebClient({timeout: this.config.webClientTimeout})
    exp.mailer = new MailerClient({
      root: this.config.mailerServer,
      key: this.config.mailerKey,
      webClient: exp.webClient
    })
    const server = this.config.sendgridServer
    const key = this.config.sendgridKey
    if ((key != null) && (key.length > 0)) {
      exp.sendgrid = new SendgridClient({
        root: server,
        key,
        webClient: exp.webClient
      })
    }
    return exp
  }

  setupRoutes (exp) {
    const appAuthc = this.appAuthc.bind(this)
    const userAuthc = this.userAuthc.bind(this)

    exp.param('couponCode', (req, res, next, code) =>
      Coupon.get(code, (err, coupon) => {
        if (err) {
          return next(err)
        } else {
          req.coupon = coupon
          return next()
        }
      })
    )

    exp.param('tokenID', (req, res, next, id) =>
      AccessToken.get(id, (err, token) => {
        if (err) {
          return next(err)
        } else {
          req.token = token
          return next()
        }
      })
    )

    exp.param('userID', (req, res, next, id) =>
      User.get(id, (err, user) => {
        if (err) {
          return next(err)
        } else {
          req.user = user
          return next()
        }
      })
    )

    const sendConfirmationEmail = function (mailer, confirmation, callback) {
      const { email } = confirmation
      const subject = 'Please confirm your fuzzy.ai account'
      const props = {confirmation}
      return mailer.send('confirmation', email, subject, props, callback)
    }

    exp.post('/register', appAuthc, appIsPrivileged, (req, res, next) => {
      let props = req.body
      props.app = req.clientApp.id

      debug(props)

      if (req.app.config.couponCodeOnly && (props.couponCode == null)) {
        next(new HTTPError('Coupon code required.', 403))
        return
      }

      if (_.isString(props.couponCode)) {
        props.couponCode = props.couponCode.toUpperCase()
      }

      return async.waterfall([
        function (callback) {
          if (props.couponCode) {
            return Coupon.maybeGet(props.couponCode, callback)
          } else {
            callback(null, null)
          }
        },
        function (coupon, callback) {
          if ((coupon == null) && (props.couponCode != null)) {
            callback(new HTTPError(`No such coupon ${props.couponCode}.`, 403))
          } else {
            callback(null)
          }
        },
        function (callback) {
          // Check to see if user exists already
          // If so, handle it in the error code below
          debug(`Checking existence of ${props.email} user`)
          return User.byEmail(props.email, (err, user) => {
            if (err && (err.name === 'NoSuchThingError')) {
              callback(null)
            } else if (err) {
              callback(err)
            } else {
              callback(new UserExistsError(props.email, user))
            }
          })
        },
        callback =>
        // Check and see if we're already confirming this.
        // If so, resend the email. If not, make a new one.
          Confirmation.get(props.email, (err, confirmation) => {
            if (err && (err.name === 'NoSuchThingError')) {
              return Confirmation.create(props, callback)
            } else if (err) {
              callback(err)
            } else {
              callback(null, confirmation)
            }
          })

      ], (err, confirmation) => {
        let mailer
        if (err) {
          if (err.name === 'UserExistsError') {
            const { user } = err;
            ({ mailer } = req.app)
            setImmediate(() => {
              const { email } = user
              const subject = 'You are already registered for Fuzzy.ai'
              props = {email}
              return mailer.send('no-need-to-register', email, subject, props, (err) => {
                if (err) {
                  return req.log.error({err}, 'Error sending login help email')
                }
              })
            })
            return res.json({status: 'awaiting email confirmation'})
          } else {
            return next(err)
          }
        } else {
          // XXX: i18n
          setImmediate(() =>
            sendConfirmationEmail(req.app.mailer, confirmation, (err) => {
              if (err) {
                const msg = 'Error sending confirmation email'
                return req.log.error({err, confirmation}, msg)
              }
            })
          )

          res.status(202)
          return res.json({status: 'awaiting email confirmation'})
        }
      })
    })

    exp.post('/confirm', appAuthc, appIsPrivileged, (req, res, next) => {
      const { code } = req.body
      let confirmation = null
      let user = null
      let token = null
      let defaultToken = null
      let couponCode = null
      return async.waterfall([
        callback =>
          Confirmation.search({code}, (err, confirmations) => {
            if (err) {
              return next(err)
            } else if (!confirmations || (confirmations.length !== 1)) {
              return next(new HTTPError(`No such code ${code}`, 400))
            } else {
              callback(null, confirmations[0])
            }
          }),
        function (results, callback) {
          confirmation = results
          debug(`Got confirmation ${confirmation}`)
          if (confirmation.confirmedAt != null) {
            callback(new HTTPError(`Code ${code} already confirmed`, 409))
          } else {
            callback(null)
          }
        },
        callback => Application.get(confirmation.app, callback),
        function (app, callback) {
          const props = {
            email: confirmation.email,
            passwordHash: confirmation.passwordHash
          }
          return User.register(props, app, callback)
        },
        function (userResults, tokenResults, defaultTokenResults, callback) {
          user = userResults
          token = tokenResults
          defaultToken = defaultTokenResults
          if (confirmation.couponCode) {
            return async.waterfall([
              callback => Coupon.get(confirmation.couponCode, callback),
              function (coupon, callback) {
                couponCode = coupon.code
                return Redemption.create({userID: user.id, code: coupon.code}, err => callback(err))
              }
            ], callback)
          } else {
            callback(null)
          }
        },
        callback =>
        // Mark it as confirmed
          confirmation.update({confirmedAt: timestamp()}, err => callback(err)),
        function (callback) {
          debug('Starting subscription stuff')
          const listName = __guard__(__guard__(req != null ? req.app : undefined, x1 => x1.config), x => x.subscriberList)
          debug(`listName=${listName}`)
          debug(`confirmation.subscribe=${confirmation.subscribe}`)
          if (confirmation.subscribe) {
            debug('Adding to list')
            debug(`req.app.sendgrid?=${(req.app.sendgrid != null)}`)
            if ((req.app.sendgrid != null) && (listName != null)) {
              return req.app.sendgrid.addToList(confirmation.email, listName, callback)
            } else {
              return req.log.warn({}, 'Not configured correctly for sendgrid')
            }
          } else {
            debug('Not adding to list')
            callback(null)
          }
        }
      ], (err) => {
        if (err) {
          return next(err)
        } else {
          const dprops = {defaultToken}
          const uprops = _.omit(user, 'passwordHash')
          const cprops = {couponCode}
          const results = _.extend(dprops, uprops, cprops)
          return res.json({user: results, token})
        }
      })
    })

    exp.post('/login', appAuthc, (req, res, next) => {
      const { email } = req.body
      const { password } = req.body
      const app = req.clientApp
      return User.login(email, password, app, (err, user, token, defaultToken, code) => {
        if (err) {
          req.log.error({err, email}, 'Error on login')
          if (err.name === 'NoSuchThingError') {
            // Check for existing confirmation - offer better error message.
            return Confirmation.search({email}, (err, confirmations) => {
              if (err || !confirmations || (confirmations.length !== 1)) {
                return next(new HTTPError('Incorrect credentials', 401))
              } else {
                return next(new HTTPError('This account needs to be confirmed.', 409))
              }
            })
          } else {
            return next(new HTTPError('Incorrect credentials', 401))
          }
        } else {
          const dprops = {defaultToken}
          const uprops = _.omit(user, 'passwordHash')
          const cprops = {couponCode: code}
          const results = _.extend(dprops, uprops, cprops)
          return res.json({user: results, token})
        }
      })
    })

    exp.post('/logout', userAuthc, (req, res, next) => {
      if ((req.token.app == null)) {
        const msg = `Can't log out default token ${req.token.token}`
        return next(new HTTPError(msg, 400))
      }

      return req.token.del((err) => {
        if (err) {
          return next(err)
        } else {
          return res.json('OK')
        }
      })
    })

    exp.get('/coupon/:couponCode', appAuthc, (req, res, next) => {
      res.set('Last-Modified', (new Date(req.coupon.updatedAt)).toUTCString())
      return res.json(req.coupon)
    })

    exp.post('/request', appAuthc, appIsPrivileged, (req, res, next) => {
      const { email } = req.body
      const newInvitation = () =>
        InvitationRequest.create({email}, (err, invitationRequest) => {
          let msg
          if (err) {
            if (err.name === 'AlreadyExistsError') {
              msg = `Address ${email} already used to request an invitation`
              return next(new HTTPError(msg, 409))
            } else {
              return next(err)
            }
          } else {
            const { mailer } = req.app
            const { supportEmail } = req.app.config
            setImmediate(() => {
              const template = 'invitation-request'
              const subject = 'New Invitation Request'
              let props = {email}
              return mailer.send(template, supportEmail, subject, props, (err) => {
                if (err) {
                  props = {err, email}
                  msg = 'Error invitation request notification'
                  return req.log.error(props, msg)
                }
              })
            })
            return res.json('OK')
          }
        })

      const existentUser = function (user) {
        const { mailer } = req.app
        setImmediate(() => {
          const template = 'already-a-user'
          const subject = "You're already a user"
          const data = {email}
          return mailer.send(template, user.email, subject, data, (err) => {
            if (err) {
              const msg = 'Error already-a-user notification'
              const props = {err, email}
              return req.log.error(props, msg)
            }
          })
        })
        return res.json('OK')
      }

      const unconfirmedUser = function (confirmation) {
        setImmediate(() =>
          sendConfirmationEmail(req.app.mailer, confirmation, (err) => {
            if (err) {
              const msg = 'Error sending confirmation email'
              return req.log.error({err, confirmation}, msg)
            }
          })
        )
        return res.json('OK')
      }

      return User.byEmail(email, (err, user) => {
        if (!err) {
          return existentUser(user)
        } else if (err.name !== 'NoSuchThingError') {
          return next(err)
        } else {
          return Confirmation.get(email, (err, confirmation) => {
            if (err && (err.name === 'NoSuchThingError')) {
              return newInvitation()
            } else if (err) {
              return next(err)
            } else {
              return unconfirmedUser(confirmation)
            }
          })
        }
      })
    })

    exp.post('/request-reset', appAuthc, appIsPrivileged, (req, res, next) => {
      let { email } = req.body
      email = email.toLowerCase()
      if (!validator.isEmail(email)) {
        return next(new HTTPError(`Invalid email: ${email}`, 400))
      }
      return async.waterfall([
        callback => User.byEmail(email, callback),
        (user, callback) => ResetConfirmation.create({userID: user.id}, callback)
      ], (err, recon) => {
        if (err) {
          if (err.name !== 'NoSuchThingError') {
            return next(err)
          } else {
            debug('No such user')
            return Confirmation.get(email, (err, confirmation) => {
              if (err && (err.name === 'NoSuchThingError')) {
                // No indication! Just pretend that it's OK
                // We don't want to leak existence/nonexistence of email
                // addresses to bad people
                return res.json({status: 'awaiting email confirmation'})
              } else if (err) {
                return next(err)
              } else {
                debug('Resending confirmation email')
                sendConfirmationEmail(req.app.mailer, confirmation, (err) => {
                  if (err) {
                    return req.log.error({err}, 'Error resending confirmation')
                  }
                })
                return res.json({status: 'awaiting email confirmation'})
              }
            })
          }
        } else {
          const { mailer } = req.app
          setImmediate(() => {
            const subject = 'Password reset request'
            const props = {confirmation: recon}
            return mailer.send('reset', email, subject, props, (err) => {
              if (err) {
                const msg = 'Error password reset request'
                return req.log.error({err, email}, msg)
              }
            })
          })
          return res.json({status: 'awaiting email confirmation'})
        }
      })
    })

    exp.post('/reset', appAuthc, appIsPrivileged, (req, res, next) => {
      const {code, password} = req.body
      let user = null
      return async.waterfall([
        callback => ResetConfirmation.get(code, callback),
        (recon, callback) => User.get(recon.userID, callback),
        function (results, callback) {
          user = results
          user.password = password
          return user.save(callback)
        },
        (saved, callback) =>
          async.parallel([
            function (callback) {
              const props = {
                userID: user.id,
                app: req.clientApp.id
              }
              return AccessToken.create(props, callback)
            },
            callback => AccessToken.default(user.id, callback)
          ], callback)

      ], (err, results) => {
        if (err) {
          return next(err)
        } else {
          const [at, dt] = Array.from(results)
          const dprops = {defaultToken: (dt != null ? dt.token : undefined)}
          const uprops = _.omit(user, 'passwordHash')
          const combined = _.extend(dprops, uprops)
          return res.json({
            user: combined,
            token: at.token
          })
        }
      })
    })

    const rc = [appAuthc, appIsPrivileged]

    exp.post('/resend-confirmation', rc, (req, res, next) => {
      let {email} = req.body
      return Confirmation.search({email}, (err, confirmations) => {
        if (err) {
          return next(err)
        } else if (!confirmations || (confirmations.length !== 1)) {
          return next(new HTTPError(`No confirmation code for ${email}`, 400))
        } else {
          const [confirmation] = confirmations

          // XXX: i18n
          const { mailer } = req.app
          setImmediate(() => {
            ({ email } = confirmation)
            const subject = 'Please confirm your fuzzy.ai account'
            const data = {confirmation}
            return mailer.send('confirmation', email, subject, data, (err) => {
              if (err) {
                const logdata = {err, confirmation}
                return req.log.error(logdata, 'Error sending confirmation email')
              }
            })
          })
          res.status(202)
          return res.json({status: 'awaiting email confirmation'})
        }
      })
    })

    // Token endpoints
    exp.get('/tokens', userAuthc, appIsPrivileged, (req, res, next) =>
      AccessToken.userTokens(req.user.id, (err, userTokens) => {
        if (err) {
          return next(err)
        } else {
          return res.json({
            status: 'OK',
            tokens: userTokens
          })
        }
      })
    )

    // TODO: deprecate this for "REST-y" option below
    exp.put('/user', userAuthc, appIsPrivileged, (req, res, next) => {
      const props = _.omit(req.body, 'passwordHash')
      return req.user.update(props, (err, user) => {
        if (err) {
          return next(err)
        } else {
          return res.json({
            status: 'OK',
            user
          })
        }
      })
    })

    exp.put('/user/:userID', appAuthc, appIsPrivileged, (req, res, next) => {
      const props = _.omit(req.body, 'passwordHash')
      return req.user.update(props, (err, user) => {
        if (err) {
          return next(err)
        } else {
          return res.json({
            status: 'OK',
            user: _.omit(user, 'passwordHash')
          })
        }
      })
    })

    const cp = [appAuthc, appIsPrivileged]

    exp.post('/user/:userID/change-password', cp, (req, res, next) => {
      const {oldPassword, newPassword} = req.body
      return async.waterfall([
        callback => bcrypt.compare(oldPassword, req.user.passwordHash, callback),
        function (passwordCorrect, callback) {
          if (!passwordCorrect) {
            callback(new PasswordError(req.user.email))
          } else {
            callback(null)
          }
        },
        callback => req.user.update({password: newPassword}, callback)
      ], (err, user) => {
        if (err) {
          return next(err)
        } else {
          return res.json({
            status: 'OK',
            user: _.omit(user, 'passwordHash')
          })
        }
      })
    })

    exp.post('/tokens', userAuthc, appIsPrivileged, (req, res, next) => {
      const {name} = req.body
      return AccessToken.userTokens(req.user.id, (err, tokens) => {
        if (err) {
          return next(err)
        } else if (_.find(tokens, {name})) {
          return next(new HTTPError('Duplicate token', 400))
        } else {
          return AccessToken.create({userID: req.user.id, name}, (err, token) => {
            if (err) {
              return next(err)
            } else {
              return res.json({
                status: 'OK',
                token
              })
            }
          })
        }
      })
    })

    exp.post('/tokens/default', userAuthc, appIsPrivileged, (req, res, next) =>
      AccessToken.default(req.user.id, (err, defaultToken) => {
        if (err) {
          return next(err)
        } else if (defaultToken) {
          return next(new HTTPError('Duplicate token', 400))
        } else {
          return AccessToken.create({userID: req.user.id}, (err, token) => {
            if (err) {
              return next(err)
            } else {
              return res.json({
                status: 'OK',
                token
              })
            }
          })
        }
      })
    )

    const gt = [userAuthc, appIsPrivileged]

    exp.delete('/tokens/:tokenID', gt, (req, res, next) =>
      req.token.del((err) => {
        if (err) {
          return next(err)
        } else { return res.json({status: 'OK'}) }
      })
    )

    exp.get('/ready', this.dontLog, this._getReady.bind(this))

    exp.get('/live', this.dontLog, (req, res, next) => res.json({status: 'OK'}))

    return exp.get('/version', (req, res, next) => res.json({name: 'authc', version}))
  }

  _getReady (req, res, next) {
    if ((this.db == null)) {
      return next(HTTPError('Database not connected', 500))
    }

    return async.parallel([
      callback => {
        // Check that we can contact the database
        return this.db.save('server-ready', 'auth', 1, callback)
      },
      callback => req.app.mailer.version(callback)
    ], (err) => {
      if (err != null) {
        debug(`${err.name}: ${err.message} in /ready`)
        return next(err)
      } else {
        return res.json({status: 'OK'})
      }
    })
  }

  getSchema () {
    const classes = [
      AccessToken,
      Application,
      Confirmation,
      Coupon,
      InvitationRequest,
      Redemption,
      ResetConfirmation,
      User
    ]
    const schema = {}
    for (const cls of Array.from(classes)) {
      schema[cls.type] = cls.schema
    }
    debug('Got schema')
    debug(schema)
    return schema
  }

  startDatabase (callback) {
    async.waterfall([
      callback => {
        // XXX: Debugging Docker Cloud service linking
        if ((this.config.driver === 'mongodb') && (this.config.params.host != null)) {
          return require('dns').lookup(this.config.params.host, callback)
        } else {
          callback(null, null, null)
        }
      },
      (address, family, callback) => {
        // Dump address to console
        if (address != null) {
          console.log(`Connecting to ${this.config.params.host} = ${address}`)
        }
        // Succeed regardless
        callback(null)
      },
      callback => {
        return Microservice.prototype.startDatabase.call(this, callback)
      },
      callback => {
        return this.ensurePrivileged(callback)
      },
      callback => {
        return this.ensureCoupon(callback)
      },
      callback => {
        return this.ensureUser(callback)
      }
    ], err => callback(err))
  }

  ensure (cls, id, props, callback) {
    return cls.get(id, (err, inst) => {
      if (!err) {
        callback(null)
      } else if (err.name === 'NoSuchThingError') {
        return cls.create(props, (err) => {
          if (err && !['AlreadyExistsError', 'EmailExistsError'].includes(err.name)) {
            callback(err)
          } else {
            callback(null)
          }
        })
      } else {
        callback(err)
      }
    })
  }

  ensurePrivileged (callback) {
    const { privileged } = this.config
    if (privileged) {
      const props = {
        id: privileged,
        name: 'Privileged',
        privileged: true
      }
      return this.ensure(Application, privileged, props, callback)
    } else {
      callback(null)
    }
  }

  ensureCoupon (callback) {
    const code = this.config.ensureCoupon
    if (code != null) {
      return this.ensure(Coupon, code, {code}, callback)
    } else {
      callback(null)
    }
  }

  ensureUser (callback) {
    const emailPassword = this.config.ensureUser
    if (emailPassword) {
      const [email, password] = Array.from(emailPassword.split(':'))
      debug(`Ensuring user ${email}`)
      return this.ensure(User, email, {email, password}, callback)
    } else {
      callback(null)
    }
  }

  stopCustom (callback) {
    this.express.webClient.stop()
    callback(null)
  }

  appAuthc (req, res, next) {
    this.bearerToken(req, (err, tokenString) => {
      if (err) {
        return next(err)
      } else {
        Application.get(tokenString, (err, app) => {
          if (err) {
            return next(err)
          } else {
            req.clientApp = app
            return next()
          }
        })
      }
    })
  }

  userAuthc (req, res, next) {
    let token = null
    return async.waterfall([
      callback => this.bearerToken(req, callback),
      (tokenString, callback) => AccessToken.get(tokenString, callback),
      function (results, callback) {
        token = results
        return async.parallel([
          callback => User.get(token.userID, callback),
          function (callback) {
            if (token.app) {
              return Application.get(token.app, callback)
            } else {
              callback(null, null)
            }
          }
        ], callback)
      }
    ], (err, results) => {
      if (err) {
        return next(err)
      } else {
        const [user, app] = Array.from(results)
        req.token = token
        req.user = user
        req.clientApp = app
        return next()
      }
    })
  }
}

module.exports = AuthServer

function __guard__ (value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined
}
