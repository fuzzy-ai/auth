// reset-confirmation.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')
const async = require('async')

const timestamp = require('./timestamp')
const randomString = require('./randomstring')

const ResetConfirmation = db.DatabankObject.subClass('ResetConfirmation')

ResetConfirmation.schema = {
  pkey: 'code',
  fields: [
    'userID',
    'app',
    'createdAt',
    'updatedAt'
  ],
  indices: ['code']
}

ResetConfirmation.beforeCreate = function (props, callback) {
  if (!props.userID) {
    callback(new Error('user ID property required'))
    return
  }

  props.createdAt = (props.updatedAt = timestamp())

  return async.waterfall([
    callback => randomString(8, callback),
    function (code, callback) {
      props.code = code
      callback(null)
    }
  ], (err) => {
    if (err) {
      callback(err)
    } else {
      callback(null, props)
    }
  })
}

ResetConfirmation.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  callback(null, props)
}

module.exports = ResetConfirmation
