// main.js
// Copyright 2014-2016 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const AuthServer = require('./authserver')

const server = new AuthServer(process.env)

// Try to send a slack message on error

process.on('uncaughtException', (err) => {
  if ((server != null) && (server.slackMessage != null)) {
    const msg = `UNCAUGHT EXCEPTION: ${err.message}`
    return server.slackMessage('error', msg, ':bomb:', (err) => {
      if (err != null) {
        return console.error(err)
      }
    })
  }
})

server.start((err) => {
  if (err) {
    return console.error(err)
  } else {
    return console.log('Server started.')
  }
})

const shutdown = function () {
  console.log('Shutting down...')
  return server.stop((err) => {
    if (err) {
      console.error(err)
      return process.exit(-1)
    } else {
      console.log('Done.')
      return process.exit(0)
    }
  })
}

process.on('SIGTERM', shutdown)
process.on('SIGINT', shutdown)
