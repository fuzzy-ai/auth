// redemption.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')

const Redemption = db.DatabankObject.subClass('Redemption')

Redemption.schema = {
  pkey: 'id',
  fields: [
    'userID',
    'code',
    'context',
    'createdAt'
  ],
  indices: ['userID', 'code']
}

Redemption.makeID = props => `${props.userID}|${props.code}`

Redemption.beforeCreate = function (props, callback) {
  if (!props.userID) {
    callback(new Error('No userID'))
  }

  if (!props.code) {
    callback(new Error('No coupon code'))
  }

  if (_.isString(props.code)) {
    props.code = props.code.toUpperCase()
  }

  props.id = Redemption.makeID(props)
  props.createdAt = timestamp()

  callback(null, props)
}

module.exports = Redemption
