// user.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const assert = require('assert')

const db = require('databank')
const async = require('async')
const bcrypt = require('bcryptjs')
const _ = require('lodash')
const validator = require('validator')

const uniqueID = require('./uniqueid')
const timestamp = require('./timestamp')
const AccessToken = require('./accesstoken')
const PasswordError = require('./password-error')
const Redemption = require('./redemption')

class EmailExistsError extends Error {
  constructor (email) {
    super(`Already have a user for ${email}`)
    this.email = email
    this.name = 'EmailExistsError'
    this.statusCode = 409
  }
}

class InvalidEmailError extends Error {
  constructor (email) {
    super(`Invalid email address ${email}`)
    this.email = email
    this.name = 'InvalidEmailError'
    this.statusCode = 500
  }
}

const User = db.DatabankObject.subClass('user')

User.schema = {
  pkey: 'id',
  fields: [
    'email',
    'plan',
    'customerID',
    'passwordHash',
    'createdAt',
    'updatedAt'
  ],
  indices: ['email']
}

const makeHash = (password, callback) =>
  async.waterfall([
    callback => bcrypt.genSalt(10, callback),
    (salt, callback) => bcrypt.hash(password, salt, callback)
  ], (err, passwordHash) => {
    if (err) {
      callback(err)
    } else {
      callback(null, passwordHash)
    }
  })

User.prototype.afterGet = function (callback) {
  if (!this.plan) {
    this.plan = 'free'
  }
  callback(null)
}

User.beforeCreate = function (props, callback) {
  if (!props.email) {
    callback(new Error('email property required'))
    return
  }

  props.id = uniqueID()
  props.createdAt = (props.updatedAt = timestamp())
  if (!props.plan) { props.plan = 'free' }

  return async.waterfall([
    function (callback) {
      if (props.email && !validator.isEmail(props.email)) {
        callback(new InvalidEmailError(props.email))
      } else {
        callback(null)
      }
    },
    callback =>
      User.byEmail(props.email, (err, user) => {
        if (!err || (err.name !== 'NoSuchThingError')) {
          callback(new EmailExistsError(props.email))
        } else {
          callback(null)
        }
      }),
    function (callback) {
      if (!props.password) {
        callback(null)
      } else {
        return makeHash(props.password, (err, passwordHash) => {
          if (err) {
            callback(err)
          } else {
            props.passwordHash = passwordHash
            delete props.password
            callback(null)
          }
        })
      }
    }
  ], (err) => {
    if (err) {
      callback(err)
    } else {
      callback(null, props)
    }
  })
}

User.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  return async.waterfall([
    function (callback) {
      if (props.email && !validator.isEmail(props.email)) {
        callback(new InvalidEmailError(props.email))
      } else {
        callback(null)
      }
    },
    function (callback) {
      if (!props.password) {
        callback(null)
      } else {
        return makeHash(props.password, (err, passwordHash) => {
          if (err) {
            callback(err)
          } else {
            props.passwordHash = passwordHash
            delete props.password
            callback(null)
          }
        })
      }
    }
  ], (err) => {
    if (err) {
      callback(err)
    } else {
      callback(null, props)
    }
  })
}

User.prototype.beforeSave = function (callback) {
  this.updatedAt = timestamp()

  if (!validator.isEmail(this.email)) {
    callback(new InvalidEmailError(this.email))
  } else if (!this.password) {
    callback(null)
  } else {
    return makeHash(this.password, (err, passwordHash) => {
      if (err) {
        callback(err)
      } else {
        this.passwordHash = passwordHash
        delete this.password
        callback(null)
      }
    })
  }
}

User.register = function (props, app, callback) {
  let user = null
  let defaultToken = null
  return async.waterfall([
    callback => User.create(props, callback),
    function (results, callback) {
      user = results
      // Default token
      return AccessToken.create({userID: user.id}, callback)
    },
    function (results, callback) {
      defaultToken = results
      return AccessToken.create({userID: user.id, app: app.id}, callback)
    }
  ], (err, appToken) => {
    if (err) {
      callback(err)
    } else {
      callback(null, user, appToken.token, defaultToken.token)
    }
  })
}

User.byEmail = (email, callback) =>
  User.search({email}, (err, users) => {
    if (err) {
      callback(err)
    } else if (!users || (users.length === 0)) {
      callback(new db.NoSuchThingError(`Could not find user for ${email}`))
    } else if (users.length > 1) {
      callback(new Error(`Too many users for ${email}`))
    } else {
      callback(null, users[0])
    }
  })

User.login = function (email, password, app, callback) {
  let user = null
  let defaultToken = null
  let appToken = null
  let couponCode = null

  assert.ok((app != null), 'app is undefined in User.login()')
  assert.ok(((app != null ? app.id : undefined) != null), 'app.id is undefined in User.login()')
  return async.waterfall([
    callback => User.byEmail(email, callback),
    function (results, callback) {
      user = results
      return bcrypt.compare(password, user.passwordHash, callback)
    },
    function (passwordCorrect, callback) {
      if (!passwordCorrect) {
        callback(new PasswordError(user.email))
      } else {
        callback(null)
      }
    },
    callback =>
      Redemption.search({userID: user.id}, (err, redemptions) => {
        if (err) {
          callback(err)
        } else if (!redemptions || (redemptions.length === 0)) {
          callback(null)
        } else {
          couponCode = redemptions[0].code
          callback(null)
        }
      }),
    callback =>
      // Find the default token and app tokens
      // XXX: This could get slow; change to use a combined key of userID and
      // appID
      AccessToken.search({userID: user.id}, (err, tokens) => {
        if (err) {
          callback(err)
        } else {
          // In case we have conflicts, show the oldest token
          tokens = _.sortBy(tokens, 'createdAt')
          defaultToken = _.find(tokens, token => _.isUndefined(token.app))
          return async.parallel([
            function (callback) {
              if (defaultToken) {
                callback(null)
              } else {
                return AccessToken.create({userID: user.id}, (err, token) => {
                  if (err) {
                    callback(err)
                  } else {
                    defaultToken = token
                    callback(null)
                  }
                })
              }
            },
            function (callback) {
              const props = {
                userID: user.id,
                app: app.id
              }
              return AccessToken.create(props, (err, token) => {
                if (err) {
                  callback(err)
                } else {
                  appToken = token
                  callback(null)
                }
              })
            }
          ], callback)
        }
      })

  ], (err) => {
    if (err) {
      callback(err)
    } else {
      assert.ok((defaultToken.app == null),
        `Default token has an app code ${defaultToken.app}`)
      assert.equal(appToken.app, app.id, 'appToken.app is not equal to app.id')
      assert.notEqual(defaultToken.token, appToken.token,
        'appToken.token equals defaultToken.token')
      callback(null, user, appToken.token, defaultToken.token, couponCode)
    }
  })
}

module.exports = User
