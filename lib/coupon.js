// coupon.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const _ = require('lodash')
const db = require('databank')

const timestamp = require('./timestamp')

const Coupon = db.DatabankObject.subClass('Coupon')

Coupon.schema = {
  pkey: 'code',
  fields: [
    'createdAt',
    'updatedAt'
  ]
}

Coupon.beforeCreate = function (props, callback) {
  if (!props.code) {
    callback(new Error('No coupon code'))
  }

  if (_.isString(props.code)) {
    props.code = props.code.toUpperCase()
  }

  props.createdAt = (props.updatedAt = timestamp())

  callback(null, props)
}

Coupon.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  callback(null, props)
}

Coupon.prototype.beforeSave = function (callback) {
  this.updatedAt = timestamp()

  callback(null)
}

Coupon.maybeGet = (id, callback) =>
  Coupon.get(id, (err, coupon) => {
    if (err) {
      if (err.name === 'NoSuchThingError') {
        callback(null, null)
      } else {
        callback(err)
      }
    } else {
      callback(null, coupon)
    }
  })

module.exports = Coupon
