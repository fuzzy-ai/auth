// confirmation.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')
const async = require('async')
const bcrypt = require('bcryptjs')
const _ = require('lodash')

const timestamp = require('./timestamp')
const randomString = require('./randomstring')

const Confirmation = db.DatabankObject.subClass('Confirmation')

Confirmation.schema = {
  pkey: 'email',
  fields: [
    'code',
    'couponCode',
    'passwordHash',
    'app',
    'subscribe',
    'confirmedAt',
    'createdAt',
    'updatedAt'
  ],
  indices: ['code']
}

Confirmation.beforeCreate = function (props, callback) {
  if (!props.email) {
    callback(new Error('email property required'))
    return
  }

  if (!props.password) {
    callback(new Error('password property required'))
    return
  }

  props.createdAt = (props.updatedAt = timestamp())

  if (_.isString(props.couponCode)) {
    props.couponCode = props.couponCode.toUpperCase()
  }

  return async.waterfall([
    callback => randomString(8, callback),
    function (code, callback) {
      props.code = code
      return bcrypt.genSalt(10, callback)
    },
    (salt, callback) => bcrypt.hash(props.password, salt, callback),
    function (passwordHash, callback) {
      props.passwordHash = passwordHash
      delete props.password
      callback(null)
    }
  ], (err) => {
    if (err) {
      callback(err)
    } else {
      callback(null, props)
    }
  })
}

Confirmation.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  callback(null, props)
}

module.exports = Confirmation
