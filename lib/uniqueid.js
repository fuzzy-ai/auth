// uniqueid.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const EPOCH = 1400000000000

const uniqueID = function () {
  const num = (((Date.now() - EPOCH) * 1000) + Math.floor(Math.random() * 1000))
  return num.toString(36)
}

module.exports = uniqueID
