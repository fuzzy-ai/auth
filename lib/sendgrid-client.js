// sendgrid-client.js
// Wrapper around the Sendgrid REST API
// Copyright 2016 Fuzzy.ai <legal@fuzzy.ai>
// All rights reserved

const async = require('async')
const debug = require('debug')('auth:sendgrid-client')
const MicroserviceClient = require('@fuzzy-ai/microservice-client')
const _ = require('lodash')

class SendgridClient extends MicroserviceClient {
  addToList (email, listName, callback) {
    debug(`addToList('${email}', '${listName}')`)
    return async.parallel([
      callback => {
        return this.post('/v3/contactdb/recipients', [{email}], (err, results) => {
          if (err) {
            callback(err)
          } else if ((results.persisted_recipients != null ? results.persisted_recipients.length : undefined) !== 1) {
            callback(new Error('Bad response from creating recipient'))
          } else {
            callback(null, results.persisted_recipients[0])
          }
        })
      },
      callback => {
        return this.get('/v3/contactdb/lists', (err, results) => {
          if (err) {
            callback(err)
          } else {
            const list = _.find(results.lists, {name: listName})
            if ((list == null)) {
              callback(new Error(`No such list ${listName}`))
            } else {
              callback(null, list.id)
            }
          }
        })
      }
    ], (err, results) => {
      if (err) {
        callback(err)
      } else {
        const [recipientID, listID] = Array.from(results)
        debug(`recipientID=${recipientID}`)
        debug(`listID=${listID}`)
        const url = `/v3/contactdb/lists/${listID}/recipients`
        return this.post(url, [recipientID], (err, results) => {
          if (err) {
            callback(err)
          } else {
            callback(null)
          }
        })
      }
    })
  }
}

module.exports = SendgridClient
