// password-error.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

class PasswordError extends Error {
  constructor (email) {
    super(`Bad password for user ${email}`)
    this.email = email
    this.statusCode = 401
  }
}

module.exports = PasswordError
