// randomstring.js
// Copyright 2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.
//
// Based on code under the Apache 2.0 license from pump.io

const crypto = require('crypto')

const randomString = (bytes, callback) =>
  crypto.randomBytes(bytes, (err, buf) => {
    if (err) {
      callback(err, null)
    } else {
      let str = buf.toString('base64')
      str = str.replace(/\+/g, '-')
      str = str.replace(/\//g, '_')
      str = str.replace(/=/g, '')
      callback(null, str)
    }
  })

module.exports = randomString
