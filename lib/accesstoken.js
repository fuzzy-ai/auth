// accesstoken.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const crypto = require('crypto')

const db = require('databank')
const _ = require('lodash')

const timestamp = require('./timestamp')

const AccessToken = db.DatabankObject.subClass('accesstoken')

AccessToken.schema = {
  pkey: 'token',
  fields: [
    'userID',
    'app',
    'name',
    'createdAt',
    'updatedAt'
  ],
  indices: ['userID', 'app']
}

AccessToken.beforeCreate = function (props, callback) {
  props.createdAt = (props.updatedAt = timestamp())

  return crypto.randomBytes(8, (err, buf) => {
    if (err) {
      callback(err, null)
    } else {
      const n1 = buf.readUInt32BE(0)
      const n2 = buf.readUInt32BE(4)
      props.token = n1.toString(36) + n2.toString(36)
      callback(null, props)
    }
  })
}

AccessToken.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  callback(null, props)
}

AccessToken.default = (id, callback) =>
  AccessToken.search({userID: id}, (err, tokens) => {
    if (err) {
      callback(err)
    } else {
      // In case we have conflicts, show the oldest token
      tokens = _.sortBy(tokens, 'createdAt')
      const defaultToken = _.find(tokens, token => _.isUndefined(token.app) && _.isUndefined(token.name))
      callback(null, defaultToken)
    }
  })

AccessToken.userTokens = (id, callback) =>
  AccessToken.search({userID: id}, (err, tokens) => {
    if (err) {
      callback(err)
    } else {
      const userTokens = _.filter(tokens, token => _.isUndefined(token.app))
      callback(null, userTokens)
    }
  })

module.exports = AccessToken
