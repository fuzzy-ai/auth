// application.js
// Copyright 2014 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const crypto = require('crypto')

const db = require('databank')

const timestamp = require('./timestamp')

const Application = db.DatabankObject.subClass('Application')

Application.schema = {
  pkey: 'id',
  fields: [
    'name',
    'url',
    'privileged',
    'createdAt',
    'updatedAt'
  ]
}

Application.beforeCreate = function (props, callback) {
  props.createdAt = (props.updatedAt = timestamp())

  if (props.id) {
    callback(null, props)
  } else {
    return crypto.randomBytes(8, (err, buf) => {
      if (err) {
        callback(err, null)
      } else {
        const n1 = buf.readUInt32BE(0)
        const n2 = buf.readUInt32BE(4)
        props.id = n1.toString(36) + n2.toString(36)
        callback(null, props)
      }
    })
  }
}

Application.prototype.beforeUpdate = function (props, callback) {
  props.updatedAt = timestamp()

  callback(null, props)
}

module.exports = Application
