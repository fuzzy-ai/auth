// invitation-request.js
// Copyright 2014,2015 9165584 Canada Corporation <legal@fuzzy.ai>
// All rights reserved.

const db = require('databank')

const timestamp = require('./timestamp')

const InvitationRequest = db.DatabankObject.subClass('InvitationRequest')

InvitationRequest.schema = {
  pkey: 'email',
  fields: [
    'createdAt'
  ]
}

InvitationRequest.beforeCreate = function (props, callback) {
  props.createdAt = timestamp()

  callback(null, props)
}

InvitationRequest.prototype.beforeUpdate = (props, callback) => callback(new Error('Immutable'))

InvitationRequest.prototype.beforeSave = callback => callback(new Error('Immutable'))

InvitationRequest.maybeGet = (id, callback) =>
  InvitationRequest.get(id, (err, InvitationRequest) => {
    if (err) {
      if (err.name === 'NoSuchThingError') {
        callback(null, null)
      } else {
        callback(err)
      }
    } else {
      callback(null, InvitationRequest)
    }
  })

module.exports = InvitationRequest
