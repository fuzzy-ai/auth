auth
====

This is the auth microservice for Fuzzy.ai.

Environment
-----------

All the same environment variables as Microservice, plus:

* AUTH_COUPON_CODE_ONLY: If set, will not allow registrations without
  a coupon code. So, functionally, makes it an "invite code".
* AUTH_PRIVILEGED_KEY: An OAuth2 application key that will be created if
  it does not already exist. Some endpoints will only work with privileged
  apps.
* AUTH_MAILER_SERVER: URL of the mailer microservice server to use.
* AUTH_MAILER_KEY: API key to use to access the mailer microservice.
* AUTH_SUPPORT_EMAIL: The email address to use in the From: part of outgoing
  emails.
* ENSURE_COUPON: A coupon code that will be created if it does not
  already exist. Primarily for testing, so don't use this in production!
* ENSURE_USER: An email:password combo that will be created if it does not
  already exist. Primarily for testing, so don't use this in production!
* SENDGRID_SERVER: Root of the Sendgrid API server to use. Defaults to the
  production server.
* SENDGRID_KEY: API key to use for Sendgrid API calls.
* SUBSCRIBER_LIST: name of the list for Fuzzy.ai users on Sendgrid.
* WEB_CLIENT_TIMEOUT: Timeout for keepalive connections. Default is
  Infinity, meaning never timeout. Can also be an integer for timeout in
  milliseconds, or 0 for no keepalive.

Endpoints
---------

The server has the following endpoints. All of them require OAuth2 for
authentication. There are two kinds of keys:

* App key: specific for an application.
* User access token: specific for a user and application.

All of the endpoints take JSON payloads.

* POST /register

  * couponCode: a valid coupon code
  * email: Email address
  * password: A password
  * subscribe: boolean flag for subscribing to the users email list

  This starts the registration process for a new user; it sends a confirmation
  email to their address, including a link to the main site with a unique
  confirmation code.

  This requires a privileged app key.

* POST /confirm

  * code: the confirmation code sent out by email

  Completes the registration process. Creates a user based on the data stored
  when the /register endpoint was called that generated this confirmation code.

  Requires a privileged app key.

  Returns a JSON object with the following properties:

  * user: the user that was registered, with:

    * id: a unique and immutable ID
    * email: unique but mutable
    * createdAt: timestamp for creation
    * updatedAt: timestamp for update (probably the same as createdAt)
    * defaultToken: A default access token not tied to any particular
      application. Give this one to the user.

  * token: the token that was generated for the calling application. Different
    from the default token! Don't give this one to the user.

* POST /login

  * email: email address of user
  * password: password of user

  Logs the user in.

  Requires app key (but not privileged).

  Returns a JSON object with the following properties:

  * user: the user that was registered, with:

    * id: a unique and immutable ID
    * email: unique but mutable
    * createdAt: timestamp for creation
    * updatedAt: timestamp for update (probably the same as createdAt)
    * defaultToken: A default access token not tied to any particular
      application. Give this one to the user.

  * token: the token that was generated for the calling application. Different
    from the default token! Don't give this one to the user.

* POST /logout

  Logs the user out. No parameters.

  Requires user access token for auth. Invalidates that token after logout!
  Can't be the default access token.

* GET /coupon/:couponCode

  Get the information about a coupon.

  Require app key.

  Returns a JSON object:

    * code: The coupon code
    * createdAt: create timestamp
    * updatedAt: update timestamp

* POST /request

  * email: email address to send the code to

  Request an invitation (coupon) code. This just sends off an email to the
  support team, asking to send this person a code.

  Requires privileged app key.

  Returns a JSON string, "OK".

* POST /request-reset

  * email: email address of user to reset the password of.

  Starts the password reset cycle. Sends off a unique key to the supplied email.

  Requires a privileged app key.

  Returns a JSON object with:

  * status: status string.

* POST /reset

  * code: the reset code
  * password: the new password

  Resets the user password for the given reset code.

  Requires a privileged app key.

  Returns a JSON object with:

  * user: the user that was registered, with:

    * id: a unique and immutable ID
    * email: unique but mutable
    * createdAt: timestamp for creation
    * updatedAt: timestamp for update (probably the same as createdAt)
    * defaultToken: A default access token not tied to any particular
      application. Give this one to the user.

  * token: the token that was generated for the calling application. Different
    from the default token! Don't give this one to the user.
