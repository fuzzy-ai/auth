FROM node:9-alpine

RUN apk add --no-cache curl

WORKDIR /opt/auth
COPY . /opt/auth

RUN npm install

EXPOSE 80
EXPOSE 443

ENTRYPOINT ["/opt/auth/bin/dumb-init", "--"]
CMD ["npm", "start"]
